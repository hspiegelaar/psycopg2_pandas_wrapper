import psycopg2
import pandas as pd

def open_db_connection():

    my_conn = None
    try:
        connstr  = "dbname='<db_name>' "
        connstr += "user='<db_user>' "
        connstr += "password='<db_password>' "
        connstr += "<db_host>'"

        my_conn = psycopg2.connect(connstr)
        my_curr = my_conn.cursor()

        my_conn.autocommit = False

        return my_conn, my_curr

    except psycopg2.DatabaseError as e:
        print('Error {}'.format(e))
        sys.exit(1)


def run_query(query):

    my_conn, my_curr = open_db_connection()
    
    try:
        my_curr.execute(query)

        result = pd.DataFrame(my_curr.fetchall())
        result.columns = [desc[0] for desc in my_curr.description]
        my_curr.close()
        my_conn.close()

        return result

    except psycopg2.DatabaseError as e:
        print('Error: {}'.format(e))
        sys.exit()
    
    
table = '<my_table>'
df = run_query('select count(1) from {}'.format(table))

df.head()
